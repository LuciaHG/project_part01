package mapreduce.one;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

public class MapPhase extends Mapper<LongWritable, Text, Text, IntWritable>  {
	    @Override
	    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

	        //StringTokenizer strTok = new StringTokenizer(value.toString(), ",");
	    	
	    	/// jackson...............
//	    	ObjectMapper mapper = new ObjectMapper();
//			
//			JsonNode root = mapper.readTree(json);
	//
//			String addr = root.get("group_addr").asText();
	    	
//	        String cols[] = value.toString().split(",");
//	        // get the state name, located in column
//	        String year = cols[2];
//	        String sex = cols[1];
//	        String newKey = year + "-" + sex;
//	        context.write(new Text(newKey), new IntWritable(1));
	    	String rawTweet = value.toString();
	    	try {
		    	
		    	
		        Status status = TwitterObjectFactory.createStatus(rawTweet);
	            String tweet = status.getText().toUpperCase();
	            if (tweet.contains("Trump")){
	            	context.write(new Text("Trump"), new IntWritable(1));
	            }
	            else if (tweet.contains("Flu")){
	            	context.write(new Text("Flu"), new IntWritable(1));
	            }
	            else if (tweet.contains("Zika")){
	            	context.write(new Text("Zika"), new IntWritable(1));
	            }
	            else if (tweet.contains("Diarrhea")){
	            	context.write(new Text("Diarrhea"), new IntWritable(1));
	            }
	            else if (tweet.contains("Ebola")){
	            	context.write(new Text("Ebola"), new IntWritable(1));
	            }
	            else if (tweet.contains("Headache")){
	            	context.write(new Text("Headache"), new IntWritable(1));
	            }
	            else if (tweet.contains("Measles")){
	            	context.write(new Text("Measles"), new IntWritable(1));
	            }
	        }
	    	catch(TwitterException e){

	        }
	    }
}
